use point::Point;
use vector::{NormalizedVector, Vector};
use object::{Material, Object, Sphere};
use sample::Sample;

#[derive(Debug, Copy, Clone)]
pub struct Light {
    shape: LightShape,
    albedo: Albedo,
}

#[derive(Debug, Copy, Clone)]
pub enum LightShape {
    SPHERE(Sphere),
    Point(Point),
}

#[derive(Debug, Clone, Copy)]
pub struct Albedo(pub f32, pub f32, pub f32);

/*fn getLo(
    p: &Point,
    n: &NormalizedVector,
    lights: &Vec<Light>,
    obs: &Vec<Object>,
    m: Material,
) -> Albedo {

}*/
