use geometric::{Point, Sphere};

#[derive(Debug, Clone)]
pub struct Cell<'a> {
    pub right: Cell_Content<'a>,
    pub left: Cell_Content<'a>,
    pub lower_corner: Point,
    pub highter_corner: Point,
}

#[derive(Debug, Clone)]
pub enum Cell_Content<'a> {
    CELL(Box<Cell<'a>>),
    SPHERE(&'a Sphere),
    EMPTY,
}

pub fn create_tree<'a>(obs: &Vec<&'a Sphere>) -> Cell<'a> {
    let mut lower_corner = Point(0.0, 0.0, 0.0);
    let mut highter_corner = Point(0.0, 0.0, 0.0);
    for ob in obs.iter() {
        let lw_sph_corner = Point(
            ob.origin.0 - ob.radius,
            ob.origin.1 - ob.radius,
            ob.origin.2 - ob.radius,
        );
        let hg_sph_corner = Point(
            ob.origin.0 + ob.radius,
            ob.origin.1 + ob.radius,
            ob.origin.2 + ob.radius,
        );
        lower_corner = Point(
            lower_corner.0.min(lw_sph_corner.0),
            lower_corner.1.min(lw_sph_corner.1),
            lower_corner.2.min(lw_sph_corner.2),
        );
        highter_corner = Point(
            highter_corner.0.max(hg_sph_corner.0),
            highter_corner.1.max(hg_sph_corner.1),
            highter_corner.2.max(hg_sph_corner.2),
        );
    }

    // get the lagest dimension of the box.
    let (x_len, y_len, z_len) = (
        highter_corner.0 - lower_corner.0,
        highter_corner.1 - lower_corner.1,
        highter_corner.2 - lower_corner.2,
    );
    let mut r_vec: Vec<&Sphere>;
    let mut l_vec: Vec<&Sphere>;
    if x_len > y_len && x_len > z_len {
        r_vec = obs.iter()
            .filter(|ob| ob.origin.0 <= lower_corner.0 + x_len / 2.0)
            .map(|&ob| ob)
            .collect();
        l_vec = obs.iter()
            .filter(|ob| ob.origin.0 >= lower_corner.0 + x_len / 2.0)
            .map(|&ob| ob)
            .collect();
    } else if y_len > z_len {
        r_vec = obs.iter()
            .filter(|ob| ob.origin.1 <= lower_corner.1 + y_len / 2.0)
            .map(|&ob| ob)
            .collect();
        l_vec = obs.iter()
            .filter(|ob| ob.origin.1 >= lower_corner.1 + y_len / 2.0)
            .map(|&ob| ob)
            .collect();
    } else {
        r_vec = obs.iter()
            .filter(|ob| ob.origin.2 <= lower_corner.2 + z_len / 2.0)
            .map(|&ob| ob)
            .collect();
        l_vec = obs.iter()
            .filter(|ob| ob.origin.2 >= lower_corner.2 + z_len / 2.0)
            .map(|&ob| ob)
            .collect();
    }
    let mut right: Cell_Content;
    let mut left: Cell_Content;
    if r_vec.is_empty() {
        right = Cell_Content::EMPTY;
    } else if r_vec.len() == 1 {
        right = Cell_Content::SPHERE(r_vec[0]);
    } else {
        right = Cell_Content::CELL(Box::new(create_tree(&r_vec)));
    }
    if l_vec.is_empty() {
        left = Cell_Content::EMPTY;
    } else if l_vec.len() == 1 {
        left = Cell_Content::SPHERE(l_vec[0]);
    } else {
        left = Cell_Content::CELL(Box::new(create_tree(&l_vec)));
    }
    Cell {
        right: right,
        left: left,
        lower_corner: lower_corner,
        highter_corner: highter_corner,
    }
}
