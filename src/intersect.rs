use point::Point;
use ray::Ray;
use ray::get_intersection_point;
use vector::{NormalizedVector, Vector};
use object::{Material, Object, Plane, Shape, Sphere};
use std::cmp::Ordering;

#[derive(Debug, Clone, Copy)]
pub struct Intersection<'a> {
    pub t: f32,
    pub p: Point,
    pub obj: &'a Object,
}

impl<'a> PartialEq for Intersection<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.t == other.t
    }
}

impl<'a> PartialOrd for Intersection<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.t.partial_cmp(&other.t)
    }
}

pub fn intersect_ray_objects<'a>(r: &Ray, obs: &'a [Object]) -> Option<Intersection<'a>> {
    let mut result: Option<Intersection> = None;
    for obj in obs {
        let oit = intersect_ray_object(r, obj);
        if let Some(it) = oit {
            if let Some(rit) = result {
                if it < rit {
                    result = oit;
                }
            }
        }
    }

    result
}

pub fn intersect_ray_object<'a>(r: &Ray, obj: &'a Object) -> Option<Intersection<'a>> {
    let mut result: Option<Intersection> = None;
    match obj.shape {
        Shape::SPHERE(sphere) => if let Some(t) = intersec_ray_sphere(r, &sphere) {
            result = Some(Intersection {
                t: t,
                p: get_intersection_point(r, t),
                obj: obj,
            });
        },
        Shape::PLANE(plane) => {}
    }
    result
}

fn intersec_ray_sphere(ray: &Ray, sphere: &Sphere) -> Option<f32> {
    //it's used several time
    let vec_or_to_os = Vector::from_two_points(&ray.origin, &sphere.origin);
    //lets get b
    let b = 2.0 * (ray.direction * vec_or_to_os);
    //since a = 1, lets get c
    let norme_vec_or_to_os = vec_or_to_os.get_norme();
    let c = norme_vec_or_to_os.powi(2) - sphere.radius.powi(2);
    //lets get delta where delta = b**2-4*ac
    let delta = b.powi(2) - 4.0 * c;
    if delta >= 0.0 {
        let sqrt_root_delta = delta.sqrt();
        let x1 = (-b + sqrt_root_delta) / 2.0;
        let x2 = (-b - sqrt_root_delta) / 2.0;
        if x2 > 0.0 && x2 < x1 {
            return Some(x2);
        } else if x1 > 0.0 {
            return Some(x1);
        } else {
            return None;
        }
    } else {
        return None;
    }
}

fn intersect_ray_plane(ray: &Ray, pl: &Plane) -> Option<f32> {
    // new coord
    // x = Px + t Dx
    // t = (x-Px)/Dx
    // first, we must find the constant dimension of pl
    /*let normalized_direction = normalize_vector(&ray.direction);
    let mut t: f32;
    if pl.normale.0 == 0.0 {
        t = (ray.origin.0 - pl.origin.0) / normalized_direction.0;
    } else if pl.normale.1 == 0.0 {
        t = (ray.origin.0 - pl.origin.0) / normalized_direction.0;
    } else {
        t = (ray.origin.0 - pl.origin.0) / normalized_direction.0;
    }
    Some(t)*/
    None
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_intersect_sphere() {
        let (ray, sphere) = (
            Ray {
                origin: Point(0.0, 0.0, 0.0),
                direction: NormalizedVector::new(&Vector(2.0, 2.0, 2.0)),
            },
            Sphere {
                origin: Point(10.0, 10.0, 10.0),
                radius: 20.0,
            },
        );

        match intersec_ray_sphere(&ray, &sphere) {
            Some(value) => assert!(value > 2.67 && value < 2.68),
            None => panic!("It should return something"),
        }
    }
}
