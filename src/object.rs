use point::Point;
use vector::{NormalizedVector, Vector};
use light::Albedo;

#[derive(Debug, Copy, Clone)]
pub struct Object {
    pub shape: Shape,
    pub material: Material,
    pub albedo: Albedo,
}

#[derive(Debug, Copy, Clone)]
pub enum Shape {
    SPHERE(Sphere),
    PLANE(Plane),
}

#[derive(Debug, Copy, Clone)]
pub struct Sphere {
    pub origin: Point,
    pub radius: f32,
}

impl Sphere {
    pub fn get_normal(&self, p: &Point) -> NormalizedVector {
        NormalizedVector::new(&Vector::from_two_points(&self.origin, p))
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Plane {
    pub origin: Point,
    pub normale: NormalizedVector,
}

#[derive(Debug, Clone, Copy)]
pub enum Material {
    DIFFUSE,
    MIRROR,
}

pub fn get_mirror_direction(i: &NormalizedVector, n: &NormalizedVector) -> NormalizedVector {
    let dir = (2.0 * ((*i) * (*n))) * ((*i) - (*n));
    NormalizedVector::new(&dir)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_get_normal_sphere() {
        let s = Sphere {
            origin: Point(0.0, 0.0, 0.0),
            radius: 3.0,
        };
        let p = Point(3.0, 3.0, 3.0);
        assert_eq!(
            s.get_normal(&p),
            NormalizedVector::new(&Vector(3.0, 3.0, 3.0))
        );
    }
}
