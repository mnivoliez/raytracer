//use image::{Color, SurfaceType};
//use data_structure::{Cell, Cell_Content};

pub fn calculate_reflect_ray(r: &Ray, p_inter: &Point, normale: &Vector) -> Ray {
    //let's get the dot product of N and I, the original ray direction
    let p = dot_product(&r.direction, &normale);
    // now, we need to scale N by the negative product and two
    let scaled_n = scale_vector(&normale, -1.0 * 2.0 * p);
    //we only need to add I now.
    let direction_new_ray = add_vector(&scaled_n, &r.direction);
    Ray {
        origin: p_inter.clone(),
        direction: direction_new_ray,
    }
}

pub fn intersect_ray_plane(ray: &Ray, pl: &Plane) -> Option<Intersection> {
    // new coord
    // x = Px + t Dx
    // t = (x-Px)/Dx
    // first, we must find the constant dimension of pl
    let normalized_direction = normalize_vector(&ray.direction);
    let mut t: f32;
    if pl.normale.0 == 0.0 {
        t = (ray.origin.0 - pl.origin.0) / normalized_direction.0;
    } else if pl.normale.1 == 0.0 {
        t = (ray.origin.0 - pl.origin.0) / normalized_direction.0;
    } else {
        t = (ray.origin.0 - pl.origin.0) / normalized_direction.0;
    }
    Some(Intersection {
        t: t,
        p: translate_point(&ray.origin, &scale_vector(&normalized_direction, t)),
    })
}

fn between(v: f32, ll: f32, hl: f32) -> bool {
    v > ll && v < hl
}

fn between_two_points(p1: &Point, p2: &Point, p3: &Point) -> bool {
    between(p1.0, p2.0, p3.0) && between(p1.1, p2.1, p3.1) && between(p1.2, p2.2, p3.2)
}

/*pub fn intersect_ray_with_cell_tree(r: &Ray, c: &Cell) -> Option<f32> {
    let r_result: Option<f32>;
    let l_result: Option<f32>;
    match c.right {
        Cell_Content::EMPTY => r_result = None,
        Cell_Content::CELL(c) => if let Some(r_t) = intersec_ray_on_cell(r, &c) {
            r_result = intersect_ray_with_cell_tree(r, &c);
        },
        Cell_Content::SPHERE(s) => if let Some(r_t) = intersec_ray_sphere(r, s) {
            r_result = Some(r_t);
        },
    }
    match c.left {
        Cell_Content::EMPTY => l_result = None,
        Cell_Content::CELL(c) => if let Some(l_t) = intersec_ray_on_cell(r, &c) {
            l_result = intersect_ray_with_cell_tree(r, &c);
        },
        Cell_Content::SPHERE(s) => if let Some(l_t) = intersec_ray_sphere(r, s) {
            l_result = Some(l_t);
        },
    }

    if let Some(r1) = r_result {
        if let Some(r2) = l_result {
            if r1 < r2 {
                return Some(r1);
            } else {
                return Some(r2);
            }
        } else {
            return Some(r1);
        }
    } else if let Some(r2) = l_result {
        return Some(r2);
    } else {
        return None;
    }
}

fn intersec_ray_on_cell(r: &Ray, c: &Cell) -> Option<f32> {
    // create both plane
    let (p1, p2) = (
        Plane {
            origin: c.lower_corner.clone(),
            normale: Vector(0.0, 1.0, 1.0),
        },
        Plane {
            origin: c.highter_corner.clone(),
            normale: Vector(0.0, 1.0, 1.0),
        },
    );

    // get result for both plane
    let (i1, i2) = (intersect_ray_plane(r, &p1), intersect_ray_plane(r, &p2));
    if let Some(inter_1) = i1 {
        if let Some(inter_2) = i2 {
            if inter_1.t > inter_2.t {
                // check that the point is inside limite
                if between_two_points(&inter_2.p, &c.lower_corner, &c.highter_corner) {
                    return Some(inter_2.t);
                }
            } else {
                if between_two_points(&inter_1.p, &c.lower_corner, &c.highter_corner) {
                    return Some(inter_1.t);
                }
            }
        } else {
            if between_two_points(&inter_1.p, &c.lower_corner, &c.highter_corner) {
                return Some(inter_1.t);
            }
        }
    } else {
        if let Some(inter_2) = i2 {
            if between_two_points(&inter_2.p, &c.lower_corner, &c.highter_corner) {
                return Some(inter_2.t);
            }
        }
    }
    None
}*/

pub fn calculate_most_closest_point<'a>(
    r: &Ray,
    spheres: &'a Vec<&Sphere>,
) -> Option<(Point, Vector, f32, &'a Sphere)> {
    let mut result: Option<(Point, Vector, f32, &'a Sphere)> = None;
    for sphere in spheres.iter() {
        match intersec_ray_sphere(&r, &sphere) {
            Some(value) => {
                let p = eval_3d(r, value);
                let normale =
                    normalize_vector(&calculate_vector_from_two_points(&p, &sphere.origin));
                //rescale a little to avoid pixel madness
                let p = translate_point(&p, &scale_vector(&normale, 0.0006));
                match result {
                    Some((_, _, t_value, _)) => if value < t_value {
                        result = Some((p, normale, value, &sphere))
                    },
                    None => result = Some((p, normale, value, &sphere)),
                }
            }
            None => (),
        }
    }
    result
}

pub fn calculate_refract_ray(
    r: &Ray,
    p_inter: &Point,
    normale: &Vector,
    ior: f32,
    enter: bool,
) -> Ray {
    let mut n1_n2: f32;
    if enter {
        n1_n2 = 1.0 / 1.0;
    }
    Ray {
        origin: Point(0.0, 0.0, 0.0),
        direction: Vector(0.0, 0.0, 0.0),
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    use rand::{thread_rng, Rng};

    fn generate_point() -> Point {
        let mut rng = thread_rng();
        Point(rng.gen::<f32>(), rng.gen::<f32>(), rng.gen::<f32>())
    }

    fn generate_vector() -> Vector {
        let mut rng = thread_rng();
        Vector(rng.gen::<f32>(), rng.gen::<f32>(), rng.gen::<f32>())
    }

    fn generate_ray() -> Ray {
        Ray {
            origin: generate_point(),
            direction: generate_vector(),
        }
    }

    fn generate_n_spheres(n: u16) -> Vec<Sphere> {
        let mut rng = thread_rng();
        let mut spheres: Vec<Sphere> = Vec::new();
        for _ in 0..n {
            spheres.push(Sphere {
                origin: generate_point(),
                radius: rng.gen(),
                color: Color(
                    rng.gen_range(0, 255),
                    rng.gen_range(0, 255),
                    rng.gen_range(0, 255),
                ),
                surface_type: SurfaceType::DIFFUSE,
            });
        }
        spheres
    }

    #[test]
    fn test_eval_3d() {
        let ray = Ray {
            origin: Point(0.0, 0.0, 0.0),
            direction: Vector(2.0, 2.0, 2.0),
        };

        let result = eval_3d(&ray, 3.0);
        let value = 2.0 / 12.0_f32.sqrt() * 3.0;
        assert_eq!(result.0, value);
        assert_eq!(result.1, value);
        assert_eq!(result.2, value);
    }

    #[test]
    fn test_calculate_most_closest_point() {
        let mut rng = thread_rng();
        let spheres = generate_n_spheres(100);
        let ray = generate_ray();
        let first_result = calculate_most_closest_point(&ray, &spheres);
        let mut spheres_2 = spheres.clone();
        let mut spheres_slice = spheres_2.as_mut_slice();
        rng.shuffle(spheres_slice);
        let spheres_2 = &Vec::from(spheres_slice);
        let second_result = calculate_most_closest_point(&ray, &spheres_2);
        match first_result {
            Some((p1, v1, f_value, s1)) => match second_result {
                Some((p2, v2, s_value, s2)) => assert_eq!(f_value, s_value),
                None => assert!(false),
            },
            None => assert!(false),
        }
    }
}
