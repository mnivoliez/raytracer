use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::cmp;

#[derive(Debug, Clone, Copy)]
pub struct Color(pub u32, pub u32, pub u32);

#[derive(Debug, Clone, Copy)]
pub enum SurfaceType {
    SPECULAR(f32),
    DIFFUSE,
    TRANSPARENT(f32)
}

fn clamp (input : u32, min : u32, max : u32) -> u32 {
    cmp::min(cmp::max(input,min), max)
}

pub fn save_img (path: &str, width : &u32, height : &u32, colors : &Vec<Color>) {
    let path = Path::new(path);
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           why.description()),
        Ok(file) => file,
    };

    let mut content : String = String::from("P3 \n");
    content.push_str(&format!("{} {} \n", width.to_string(), height.to_string()));
    content.push_str("255");

    for color in colors {
        content.push_str(&format!("\n{} {} {}", 
            clamp(color.0, 0, 255), 
            clamp(color.1, 0, 255), 
            clamp(color.2, 0, 255)).to_string());
    }
    
    match file.write_all(content.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

pub fn lerp(v0: f32, v1: f32, t: f32) -> f32 {
    (1.0 - t) * v0 + t * v1
}

#[test]
fn test_save_img() {
    let path : &'static str = "test.ppm";
    let mut v: Vec<Color> = Vec::new();
    let (width, height) = (5, 5);
    for i in 0..height {
        for j in 0..width {
            v.push(
                Color(i*255/height, 
                    0, 
                    j*255/width))
        }
    }
    save_img(path, &width, &height, &v);
    
    let path = Path::new(path);
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        // The `description` method of `io::Error` returns a string that
        // describes the error
        Err(why) => panic!("couldn't open {}: {}", display,
                                                   why.description()),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut content = String::new();
    match file.read_to_string(&mut content) {
        Err(why) => panic!("couldn't read {}: {}", display,
                                                   why.description()),
        Ok(_) => assert_eq!(content, "P3 \n5 5 \n255\n0 0 0\n0 0 51\n0 0 102\n0 0 153\n0 0 204\n51 0 0\n51 0 51\n51 0 102\n51 0 153\n51 0 204\n102 0 0\n102 0 51\n102 0 102\n102 0 153\n102 0 204\n153 0 0\n153 0 51\n153 0 102\n153 0 153\n153 0 204\n204 0 0\n204 0 51\n204 0 102\n204 0 153\n204 0 204"),
    }
}

#[test]
fn test_lerp() {
    let (v0, v1, t) = (3.0, 2.0, 0.0);
    assert_eq!(lerp(v0, v1, t), 3.0);
    let (v0, v1, t) = (3.0, 2.0, 1.0);
    assert_eq!(lerp(v0, v1, t), 2.0);
    let (v0, v1, t) = (3.0, 2.0, 0.5);
    assert_eq!(lerp(v0, v1, t), 2.5);
}
