use point::Point;
use vector::{NormalizedVector, Vector};
use ray::Ray;
use object::Object;
use light::Light;

use std::f32;

use rand::random;

pub struct Camera {
    pub origin: Point,
    pub fov: f32,
}

pub struct Scene {
    pub width: u32,
    pub height: u32,
    pub camera: Camera,
    pub objects: Vec<Object>,
    pub lights: Vec<Light>,
}

/*fn color_incident_ray_on_spheres(
    r: &Ray,
    spheres: &Vec<&Sphere>,
    lights: &Vec<Light>,
    n: u8,
) -> Color {
    match calculate_most_closest_point(r, spheres) {
        Some((ref p_inter, ref normale, _, ref sphere)) => if n < 50 {
            match sphere.surface_type {
                SurfaceType::SPECULAR(ioreflect) => {
                    let n_r = calculate_reflect_ray(r, p_inter, normale);
                    let c1 = color_incident_ray_on_spheres(&n_r, spheres, lights, n + 1);
                    let color = reflect_color(&sphere.color, &c1, ioreflect);
                    color
                }
                SurfaceType::TRANSPARENT(ior) => {
                    //let n_r = calculate_refract_ray(r, p_inter, normale, ior, true);
                    Color(1, 1, 1)
                }
                SurfaceType::DIFFUSE => {
                    let s_color = shade(&sphere.color, p_inter, normale, spheres, lights);
                    s_color
                }
            }
        } else {
            sphere.color.clone()
        },
        None => Color(0, 0, 0),
    }
}

fn reflect_color(c1: &Color, c2: &Color, ratio: f32) -> Color {
    Color(
        lerp(c1.0 as f32, c2.0 as f32, ratio).round() as u32,
        lerp(c1.1 as f32, c2.1 as f32, ratio).round() as u32,
        lerp(c1.2 as f32, c2.2 as f32, ratio).round() as u32,
    )
}

fn shade(c: &Color, p: &Point, n: &Vector, spheres: &Vec<Sphere>, lights: &Vec<Light>) -> Color {
    let mut intensity = 0.0;
    for l in lights {
        intensity += calculate_intensity_from_one_light(p, n, spheres, l)
    }
    Color(
        (c.0 as f32 * intensity) as u32,
        (c.1 as f32 * intensity) as u32,
        (c.2 as f32 * intensity) as u32,
    )
}

fn calculate_intensity_from_one_light(
    p: &Point,
    n: &Vector,
    spheres: &Vec<Sphere>,
    l: &Light,
) -> f32 {
    let mut l_intensity = 0.0;
    for _ in 0..50 {
        //get random point from light
        //get 2 random points
        let (r1, r2) = (random::<f32>(), random::<f32>());
        let light_point = Point(
            l.origin.0 +
                2.0 * l.radius * (2.0 * f32::consts::PI * r1).cos() * (r2 * (1.0 - r2)).sqrt(),
            l.origin.1 +
                2.0 * l.radius * (2.0 * f32::consts::PI * r1).sin() * (r2 * (1.0 - r2)).sqrt(),
            l.origin.2 + l.radius * (1.0 - 2.0 * r2),
        );
        let direction = calculate_vector_from_two_points(&light_point, p);
        let normalized = normalize_vector(&direction);
        let d_light_point = calculate_norme_vector(&direction);
        let r = Ray {
            origin: p.clone(),
            direction: normalized.clone(),
        };
        let mut l_blocked = false;
        for s in spheres {
            match intersec_ray_sphere(&r, &s) {
                Some(value) => {
                    //from value, get the point of contact
                    let p_c = eval_3d(&r, value);
                    //get verctor from ray origin to point of contact
                    let v = calculate_vector_from_two_points(&r.origin, &p_c);
                    //get norm of it
                    let n = calculate_norme_vector(&v);
                    if n < d_light_point {
                        l_blocked = true;
                        break;
                    }
                }
                None => continue,
            }
        }
        if !l_blocked {
            l_intensity += l.intensity * dot_product(n, &normalized).abs() / d_light_point.powi(2);
        }
    }
    l_intensity / 20.0
}
*/
