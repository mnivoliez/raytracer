use scene::Scene;
use object::Object;
use ray::Ray;
use light::{Albedo, Light};
use intersect::intersect_ray_objects;

/*pub fn render(scene: &Scene) -> Vec<Vec<f32>> {
    let mut v: Vec<Color> = Vec::new();
    let (f_width, f_height) = (scene.width as f32, scene.height as f32);
    for i in 1..scene.height + 1 {
        let f_i = i as f32;
        for j in 1..scene.width + 1 {
            let f_j = j as f32;

            let ray = Ray {
                origin: scene.camera.origin.clone(),
                direction: Vector(
                    f_j - f_width / 2.0,
                    f_i - f_height / 2.0,
                    -f_width / (2.0 * (scene.camera.fov.to_radians() / 2.0).tan()),
                ),
            };
            v.push(color_incident_ray_on_spheres(
                &ray,
                &scene.objects.iter().collect(),
                &scene.lights,
                0,
            ));
        }
    }
    v
}*/

fn radiance(r: &Ray, obs: &Vec<Object>, lights: &Vec<Light>, depth: u32) -> Albedo {
    if depth == 5 {
        return Albedo(0.0, 0.0, 0.0);
    }

    let it = intersect_ray_objects(r, obs);
    Albedo(0.0, 0.0, 0.0)
}
