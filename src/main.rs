extern crate rand;

mod vector;
mod point;
mod intersect;
mod ray;
mod object;
mod light;
mod render;
mod sample;
mod scene;
/*mod image;
mod geometric;
mod scene;
mod data_structure;

use geometric::{Point, Sphere};
use image::{Color, SurfaceType};
use scene::calculate_scene;

use scene::{Camera, Light, Scene};*/

/// Use `cargo test` to execute all tests and `cargo run`
fn main() {
    /*let path: &'static str = "scene_1.ppm";
    let scene = scene_1();
    let v = calculate_scene(&scene);
    image::save_img(path, &scene.width, &scene.height, &v);*/
}
/*
fn scene_1() -> Scene {
    let mut scene = Scene {
        width: 1024,
        height: 1024,
        camera: Camera {
            origin: Point(0.0, 0.0, 0.0),
            fov: 80.0,
        },
        spheres: Vec::new(),
        lights: Vec::new(),
    };
    scene.spheres.push(Sphere {
        origin: Point(20.0, -30.0, -55.0),
        radius: 10.0,
        color: Color(1, 255, 1),
        surface_type: SurfaceType::SPECULAR(0.8),
    });
    scene.spheres.push(Sphere {
        origin: Point(20.0, -30.0, -85.0),
        radius: 10.0,
        color: Color(255, 255, 255),
        surface_type: SurfaceType::SPECULAR(0.8),
    });
    scene.spheres.push(Sphere {
        origin: Point(-20.0, -30.0, -55.0),
        radius: 10.0,
        color: Color(1, 255, 1),
        surface_type: SurfaceType::SPECULAR(0.8),
    });
    scene.spheres.push(Sphere {
        origin: Point(-20.0, -30.0, -85.0),
        radius: 10.0,
        color: Color(255, 255, 255),
        surface_type: SurfaceType::SPECULAR(0.8),
    });
    scene.spheres.push(Sphere {
        origin: Point(20.0, 30.0, -55.0),
        radius: 10.0,
        color: Color(1, 255, 1),
        surface_type: SurfaceType::SPECULAR(0.8),
    });
    scene.spheres.push(Sphere {
        origin: Point(20.0, 30.0, -85.0),
        radius: 10.0,
        color: Color(255, 255, 255),
        surface_type: SurfaceType::SPECULAR(0.8),
    });
    scene.spheres.push(Sphere {
        origin: Point(-20.0, 30.0, -55.0),
        radius: 10.0,
        color: Color(1, 255, 1),
        surface_type: SurfaceType::SPECULAR(0.8),
    });
    scene.spheres.push(Sphere {
        origin: Point(-20.0, 30.0, -85.0),
        radius: 10.0,
        color: Color(255, 255, 255),
        surface_type: SurfaceType::SPECULAR(0.8),
    });
    //roof
    scene.spheres.push(Sphere {
        origin: Point(0.0, -2000.0 - 100.0, 0.0),
        radius: 2000.0,
        color: Color(0, 0, 255),
        surface_type: SurfaceType::DIFFUSE,
    });
    //floor
    scene.spheres.push(Sphere {
        origin: Point(0.0, 2000.0 + 100.0, 0.0),
        radius: 2000.0,
        color: Color(0, 255, 0),
        surface_type: SurfaceType::DIFFUSE,
    });
    //left wall
    scene.spheres.push(Sphere {
        origin: Point(-2000.0 - 75.0, 0.0, 0.0),
        radius: 2000.0,
        color: Color(255, 0, 0),
        surface_type: SurfaceType::DIFFUSE,
    });
    //right wall
    scene.spheres.push(Sphere {
        origin: Point(2000.0 + 75.0, 0.0, 0.0),
        radius: 2000.0,
        color: Color(255, 255, 0),
        surface_type: SurfaceType::DIFFUSE,
    });
    //back wall
    scene.spheres.push(Sphere {
        origin: Point(0.0, 0.0, -2000.0 - 200.0),
        radius: 2000.0,
        color: Color(255, 255, 255),
        surface_type: SurfaceType::SPECULAR(1.0),
    });
    //lights
    scene.lights.push(Light {
        origin: Point(0.0, -20.0, 0.0),
        radius: 5.0,
        intensity: 2000.0,
    });
    scene.lights.push(Light {
        origin: Point(20.0, 0.0, -75.0),
        radius: 5.0,
        intensity: 500.0,
    });
    scene.lights.push(Light {
        origin: Point(-20.0, 0.0, -75.0),
        radius: 5.0,
        intensity: 500.0,
    });
    scene
}
*/
