use point::Point;
use vector::{NormalizedVector, Vector};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Ray {
    pub origin: Point,
    pub direction: NormalizedVector,
}

pub fn offset_ray(ray: &Ray, normal: &NormalizedVector) -> Ray {
    Ray {
        origin: ray.origin + 0.001 * normal.0,
        direction: ray.direction,
    }
}

pub fn get_intersection_point(r: &Ray, t: f32) -> Point {
    r.origin + t * r.direction
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_get_intersection_point() {
        let r = Ray {
            origin: Point(1.0, 1.0, 1.0),
            direction: NormalizedVector::new(&Vector(2.0, 3.0, 4.0)),
        };

        let p = get_intersection_point(&r, 5.0);
        assert_eq!(
            p,
            Point(
                1.0 + 5.0 * 2.0 / 29.0_f32.sqrt(),
                1.0 + 5.0 * 3.0 / 29.0_f32.sqrt(),
                1.0 + 5.0 * 4.0 / 29.0_f32.sqrt()
            )
        );
    }

    #[test]
    fn test_offset_ray() {
        let r = Ray {
            origin: Point(1.0, 1.0, 1.0),
            direction: NormalizedVector::new(&Vector(2.0, 3.0, 4.0)),
        };

        let nv = NormalizedVector(Vector(1.0, 1.0, 1.0));
        assert_eq!(
            Ray {
                origin: Point(1.001, 1.001, 1.001),
                direction: NormalizedVector::new(&Vector(2.0, 3.0, 4.0)),
            },
            offset_ray(&r, &nv)
        );
    }
}
