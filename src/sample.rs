use vector::{NormalizedVector, Vector};
use std::f32::consts::PI;

#[derive(Debug, Copy, Clone)]
pub struct Sample<T> {
    pub sample: T,
    pub pdf: f32,
}

fn sample_uniform_sphere(u: f32, v: f32) -> Sample<NormalizedVector> {
    let pi2u = 2.0 * PI * u;
    let sqrtv1minusv = (v * (1.0 - v)).sqrt();
    Sample {
        sample: NormalizedVector::new(&Vector(
            2.0 * pi2u.cos() * sqrtv1minusv,
            2.0 * pi2u.sin() * sqrtv1minusv,
            1.0 - 2.0 * v,
        )),
        pdf: 1.0 / (4.0 * PI),
    }
}

fn sample_uniform_hemisphere(u: f32, v: f32) -> Sample<NormalizedVector> {
    let pi2u = 2.0 * PI * u;
    let sqrt1minusvv = (1.0 - v.powi(2)).sqrt();
    Sample {
        sample: NormalizedVector::new(&Vector(
            pi2u.cos() * sqrt1minusvv,
            pi2u.sin() * sqrt1minusvv,
            v,
        )),
        pdf: 1.0 / (2.0 * PI),
    }
}

fn sample_uniform_hemisphere_cos(u: f32, v: f32) -> Sample<NormalizedVector> {
    let pi2u = 2.0 * PI * u;
    let sqrt1minusv = (1.0 - v).sqrt();
    let sqrtv = v.sqrt();
    Sample {
        sample: NormalizedVector::new(&Vector(
            pi2u.cos() * sqrt1minusv,
            pi2u.sin() * sqrt1minusv,
            sqrtv,
        )),
        pdf: sqrtv * PI,
    }
}
